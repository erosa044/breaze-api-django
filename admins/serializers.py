from rest_framework import serializers
from .models import (Themes,
                    Configuration)

class ThemesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Themes
        fields = '__all__'

class ConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Configuration
        fields = '__all__'
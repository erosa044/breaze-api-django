from django.conf import settings
import rest_framework
from rest_framework import generics, status, viewsets, filters, permissions, response
from real_estate.serializers import (AgentSerializer,
                                     PropertySerializer,
                                     PropertyMediaSerializer,
                                     PropertyDetailSerializer,
                                     PropertyLocationSerializer,
                                     PropertyTagSerializer,
                                     ThemeSerializer,
                                     ListSerializer,
                                     SavedSearchSerializer,
                                     OpenHouseSerializer,
                                     BHGeometrySerializer,
                                     SearchHistorySerializer)
from real_estate.mixins import AllowPUTAsCreateMixin
from real_estate.models import (Agent,
                                Property,
                                PropertyMedia,
                                PropertyDetail,
                                PropertyLocation,
                                Tag,
                                Theme,
                                List,
                                SavedSearch,
                                OpenHouse,
                                BHGeometry,
                                SearchHistory)
from admins.models import Configuration

from real_estate.filters import (PropertyFilter,
                                 SavedSearchFilter,
                                 SearchHistoryFilter)

from rest_framework.decorators import list_route,detail_route
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from rest_framework.response import Response
from real_estate.filters import (PropertyFilter)
from rest_framework.decorators import list_route
from django.contrib.gis.geos import GEOSGeometry
from django.db.models import Count
from django.db.models.query import EmptyQuerySet
from itertools import chain
from django.db.models import Q
import urllib
import json
from django.http import JsonResponse
from rest_framework.pagination import LimitOffsetPagination
from real_estate.polygons import Polygons
from real_estate.utilities import Utilities
import sys

# BEGIN Import Django email lib: Adrian ahern471@fiu.edu
from django.core.mail import send_mail
from breaze.settings import EMAIL_HOST_USER
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# END Import Django email lib

class AgentViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows details to be viewed or edited.
    """
    serializer_class = AgentSerializer
    ordering_fields = '__all__'
    pagination_class = None
    queryset = Agent.objects.all()

class PropertyViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows properties to be viewed or edited.

    Sub resource are PropertyMedia and PropertyDetail which can be found at
    `/property/{id}/media` and `/property/{id}/detail` respectively.

    Fields with filters enabled are:

    ```
      1. 'current_price'
      2. 'beds_total'
      3. 'for_sale_yn'
      4. 'for_lease_yn'
      5. 'property_type'
      6. 'matrix_unique_id'
      7. 'sq_ft_total'
      8. 'year_built'
      9. 'lot_sq_footage'
      10. 'pets_allowed_yn'
      11. 'status'
      12. 'city'
      13. 'location_point'
      14. 'address_internet_display'
      15. 'postal_code'
      16. 'state_or_province'
    ```
    """
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
    ordering_fields = '__all__'
    filter_class = PropertyFilter
    pagination_class = LimitOffsetPagination

    #Ashif's Code . Merge this Overridden get_queryset method only , ignore other new methods below this . 
    """
        Overwrite get queryset to check if get openhouse only
    """
    def get_queryset(self):
        queryset = super().get_queryset()      
        isopenhouse = self.request.query_params.get('isopenhouse', None) 
        if isopenhouse is not None:
           queryset = queryset.filter(open_house_upcoming__startswith="Public")
        return queryset

    #BEGIN PoLR Search API Implementation
    """
        Takes a search text query string and returns a list of possible suggestions after geographically whittling down the list to properties reachable in a certain amount of time, from a certain place, on a given day/time of the week
    """
    @list_route()
    def polr_properties(self, request):
        
        polr_settings = Configuration.objects.get(pk=1)

        #as of 11/5, the snippet below is the following isochrone:  Thurs, 5 PM, 25 mins (12 Angle Resolution), 11200 SW 8th St, Miami, FL 33199
        #isochrone = GEOSGeometry('POLYGON((-80.3751759 25.911687097606865,-80.28473648804989 25.898918989833717,-80.24774816235096 25.824201546038474,-80.19851276692452 25.757927101646676,-80.28230525890234 25.709693429825073,-80.30593866521605 25.6499125259134,-80.3751759 25.514045693396184,-80.44486951364652 25.649199082665632,-80.48802435212174 25.699281908971468,-80.62076348098006 25.7578276979772,-80.49289227745373 25.819165354409773,-80.42851107061833 25.841165849790585,-80.3751759 25.911687097606865))')
        isochrone = GEOSGeometry("POLYGON((" + request.GET.get('polr', "") + "))")
        
        polr_properties = Property.objects.filter(location_point__coveredby=isochrone)
        
        # get price filter range parameters 
        price_high = request.GET.get('current_price__lte')
        price_low = request.GET.get('current_price__gte')

        #use values to filter by price range
        polr_properties = polr_properties.filter(current_price__lte=price_high)
        polr_properties = polr_properties.filter(current_price__gte=price_low)


        #dyanmic filter stacker except  polr and price
        for key, value in request.GET.items():
            if str(key) != "polr" and str(key) != "current_price__lte" and key != "current_price__gte":
                command = "polr_properties = polr_properties.filter(" + str(key) + "=" + str(value) + ")\n"
               # sys.stderr.write(command)
                exec(command, globals(), locals())

        serializer = PropertySerializer(polr_properties, many=True)

        page = self.paginate_queryset(polr_properties)
        if page is not None:
            serializer = PropertySerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return response.Response(serializer.data)

    #END PoLR Search API Implementation 
    
    """
        Takes a polygon with longitude and then latitude
    """
    @list_route()
    def poly_properties(self, request):
        #geom = GEOSGeometry('POLYGON(( -80.3653335571289 25.743003105825977, -80.23761749267578 25.743003105825977,-80.23761749267578 25.82523468800373,-80.3653335571289 25.82523468800373,-80.3653335571289 25.743003105825977))')
        geom = GEOSGeometry(request.query_params['poly'])
        #poly_properties = PropertyLocation.objects.filter(point__coveredby=geom)
        poly_properties = Property.objects.filter(location_point__coveredby=geom)
        #serializer = PropertyLocationSerializer(poly_properties, many=True)
        serializer = PropertySerializer(poly_properties, many=True)

        page = self.paginate_queryset(poly_properties)
        if page is not None:
            #serializer = PropertyLocationSerializer(page, many=True)
            serializer = PropertySerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return response.Response(serializer.data)

    """
        Takes a search text query string and returns a list of possible suggestions
    """
    @list_route()
    def search_location_ajax(self, request):
        
        address = request.GET.get('address', "") #request.query_params['address']
        zipcode = request.GET.get('zip', "") #request.query_params['zip']
        city = request.GET.get('city', "") #request.query_params['city']
        #state = request.GET.get('state',"")
        properties = {}
        if len(address) > 1 and len(zipcode) > 1 and len(city) > 1:
            properties = Property.objects.filter( (Q(address_internet_display__istartswith=address) & Q(city__istartswith=city) & Q(postal_code__istartswith=zipcode)) ).values("address_internet_display", "city", "postal_code", "state_or_province") 
        elif len(address) > 1 and len(zipcode) > 1:
            properties = Property.objects.filter( (Q(address_internet_display__istartswith=address) & ~Q(city__iexact=city) & Q(postal_code__istartswith=zipcode)) ).values("address_internet_display", "city", "postal_code", "state_or_province")  
        elif len(city) > 1 and len(zipcode) > 1:
            properties = Property.objects.filter( (~Q(address_internet_display__iexact=address) & Q(city__istartswith=city) & Q(postal_code__istartswith=zipcode)) ).values("address_internet_display", "city", "postal_code", "state_or_province").distinct("city")    
        elif len(address) > 1 and len(city) > 1:
            properties = Property.objects.filter( (Q(address_internet_display__istartswith=address) & Q(city__istartswith=city) & ~Q(postal_code__iexact=zipcode)) ).values("address_internet_display", "city", "postal_code", "state_or_province")  
        elif len(address) > 1:
            properties = Property.objects.filter( (Q(address_internet_display__istartswith=address) & ~Q(city__iexact=city) & ~Q(postal_code__iexact=zipcode)) ).values("address_internet_display", "city", "postal_code", "state_or_province")  
        elif len(city) > 1:
            properties = Property.objects.filter( (~Q(address_internet_display__iexact=address) & Q(city__istartswith=city) & ~Q(postal_code__iexact=zipcode)) ).values("city", "postal_code", "state_or_province").distinct("city")  
        elif len(zipcode) > 1:
            properties = Property.objects.filter( (~Q(address_internet_display__iexact=address) & ~Q(city__iexact=city) & Q(postal_code__istartswith=zipcode)) ).values("city", "postal_code", "state_or_province").distinct("city")  

        return JsonResponse({'suggestions': list(properties)})

    # BEGIN Share property via email: Adrian ahern471@fiu.edu
    @list_route()
    def share_via_email(self, request):
        
        print('share_via_email called')

        if request.method == "GET":

            recipient_email = request.GET.get('recipient_email', "")
            sender_email    = request.GET.get('sender_email', "")
            email_body      = request.GET.get('email_body', "")
            property_url    = request.GET.get('property_url', "")
            subject         = sender_email + ' shared a home on BreazeHome'

            # email_body = email_body + ' ' + property_url

            print('email body = ' + email_body)
            print('sender email = ' + sender_email)
            print('recipient email = ' + recipient_email)

        else:
            # bad request
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # Create the body of the message (a plain-text and an HTML version).
    
        html = """\
             <!DOCTYPE html>
            <html>
                <head></head>
                <body>
                    <h1>Shared Listing</h1>
                    <p><b>""" + '"' + email_body + '"' + """</b></p>
                    <p><a href=""" + property_url + """ target="_top">See on Breaze Home</a></p><br><br>
                    <p style="color:gray;"><font size="3">Shared by:</font></p>
                    <p><a href="mailto:""" + sender_email + '"'+ """ target="_top">"""+sender_email + """</a></p>
                </body>
            </html>

        """
        print(html)
        send_mail(
            subject,
            email_body,
            EMAIL_HOST_USER,
            [recipient_email],
            fail_silently=False,
            html_message=html
        )    
            
        return Response(status=status.HTTP_200_OK)
    # END Share property via email

class PropertyDetailViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows details to be viewed or edited.
    """
    serializer_class = PropertyDetailSerializer
    ordering_fields = '__all__'
    pagination_class = None

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['property_pk']
        return PropertyDetail.objects.filter(property=pk)

class PropertyMediaViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    serializer_class = PropertyMediaSerializer
    ordering_fields = '__all__'

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['property_pk']
        return PropertyMedia.objects.filter(property=pk)


class PropertyLocationViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows locaitons to be viewed or edited.
    """
    serializer_class = PropertyLocationSerializer
    ordering_fields = '__all__'
    pagination_class = None

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['property_pk']
        return PropertyLocation.objects.filter(property=pk)

class PropertyTagViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    serializer_class = PropertyTagSerializer
    ordering_fields = '__all__'

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['property_pk']
        return Tag.objects.order_by("tag").filter(pid=pk).distinct('tag')
        
class TagViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows media to be viewed or edited.
    """
    serializer_class = PropertyTagSerializer
    ordering_fields = '__all__'
    queryset = Tag.objects.all()

    @list_route()
    def search(self, request):
        tags = request.query_params['tag']
        tag_properties = Tag.objects.filter(tag=tags)
        serializer = PropertyTagSerializer(tag_properties, many=True)

        page = self.paginate_queryset(tag_properties)
        if page is not None:
            serializer = PropertyTagSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return response.Response(serializer.data)

    @list_route()
    def user(self, request):
        tags = request.query_params['tag']
        userid = request.query_params['uid']
        tag_properties = Tag.objects.filter(tag=tags,uid=userid)
        serializer = PropertyTagSerializer(tag_properties, many=True)

        page = self.paginate_queryset(tag_properties)
        if page is not None:
            serializer = PropertyTagSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return response.Response(serializer.data)
class ListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    serializer_class = ListSerializer
    ordering_fields = '__all__'

    @detail_route(methods=['POST'])
    def save(self, request, user_pk, pk=None):
        # import ipdb
        # ipdb.set_trace()
        list_instance = List.objects.get(user=user_pk)
        property_instance = Property.objects.get(pk=request.data['property'])
        list_instance.properties.add(property_instance)
        return Response({'status': 'property added'})


    def get_queryset(self):
        """
        This view should return a list of all the List for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['user_pk']
        return List.objects.filter(user=pk)


class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    ordering_fields = '__all__'
    serializer_class = ThemeSerializer

class SavedSearchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows searches to be viewed or edited.

    Fields with filters enabled are:

    ```
      1. 'title'
      2. 'price'
      3. 'beds_minimum'
      4. 'property_type'
      5. 'transaction_type'
    ```
    """
    serializer_class = SavedSearchSerializer
    ordering_fields = '__all__'
    filter_class = SavedSearchFilter


    def get_queryset(self):
        """
        This view should return a list of all the searches for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['user_pk']
        return SavedSearch.objects.filter(user=pk)

class OpenHouseViewSet(AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows details to be viewed or edited.
    """
    serializer_class = OpenHouseSerializer
    ordering_fields = '__all__'
    pagination_class = None
    queryset = OpenHouse.objects.all()

#BEGIN PoLR Geometry API Implementation 

class BHGeometryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows for invocation of geometric utilities.
    """


    serializer_class = BHGeometrySerializer
    ordering_fields = None
    pagination_class = None
    queryset = BHGeometry.objects.none()  #this is a service logic ViewSet, hence there will be no results coming from the database for this release

    @list_route()
    def get_geocode(self, request):
        """
        This returns the geocode for a given address string.  It is a wrapper to the internal get_geocode() functionality used for other coding 
        and represents a convenience forwarded to API callers for the sake of consitency in the entire API and front ends.
        """      
        polr_settings = Configuration.objects.get(pk=1)
        return JsonResponse({ "originGeocode" : Utilities.get_geocode(request.GET.get("origin_address", ""), polr_settings) })    

    @list_route()
    def get_polr(self, request):
        """
        This returns the PoLR JSON data structure consisting of a geocode for a given address string and an isochrone represented by all points reachable 
        from the origin geocode in the specified travel_duration for a given travel_mode.  The isochrone is formed by connecting isopoints with isolines
        to enclose the area of the polygon.
        """      

        polr_settings = Configuration.objects.get(pk=1)
        origin_geocode = Utilities.get_geocode(request.GET.get("origin_address", ""), polr_settings)
        isochrone = Polygons.get_isochrone(origin_geocode, request.GET.get("arrival_datetime", ""), request.GET.get("travel_duration", ""), request.GET.get("travel_mode", "driving"), polr_settings)  #default to driving, add more arguments as necessary here for supporting additional isochrone generation parameters.

        return JsonResponse({"originGeocode" : origin_geocode, "isochrone" : isochrone})    
    
#END PoLR Geometry API Implementation

class SearchHistoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows searches history to be viewed or edited.

    Fields with filters enabled are:

    ```
      1. 'id'
      2. 'time_created'
      3. 'count'
      4. 'query_string'
    ```
    """
    serializer_class = SearchHistorySerializer
    ordering_fields = '__all__'
    filter_class = SearchHistoryFilter


    def get_queryset(self):
        """
        This view should return a list of all the searches history for
        the user as determined by the username portion of the URL.
        """
        pk = self.kwargs['user_pk']
        return SearchHistory.objects.filter(user=pk)
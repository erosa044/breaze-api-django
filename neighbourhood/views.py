from django.shortcuts import get_object_or_404
from rest_framework import generics
#from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Hazard
from .serializers import HazardSerializer
from django_filters import rest_framework as filters
from neighbourhood.filters import EventFilter
# Create your views here.

#hazards/
class HazardList(generics.ListAPIView):
    queryset = Hazard.objects.all()
    serializer_class=HazardSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EventFilter

    #def get(self, request):
    #    hazards = Hazard.objects.all()
    #    serializer = HazardSerializer(hazards, many=True)
    #    return Response(serializer.data)

    #def post(self):
    #    pass

from django.db import models
from django.contrib.auth.models import AbstractUser
import real_estate.models

class User(AbstractUser):
    email = models.EmailField(
        unique=True,
        null=True
    )
    REQUIRED_FIELDS = ['email']

    reset_token = models.CharField(
        max_length=6,
        null=True,
        blank=True
    )

    phone_number = models.CharField(
        max_length=32,
        null=True,
        blank=True
    )


class Profile(models.Model):
    GENDER_CHOICES = (
        ('U', 'UNKNOWN'),
        ('M', 'MALE'),
        ('F', 'FEMALE')
    )

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    bg_img = models.URLField(
        null=True,
        blank=True
    )

    first_name = models.CharField(
        max_length=45,
        null=True,
        blank=True
    )

    last_name = models.CharField(
        max_length=45,
        null=True,
        blank=True
    )

    gender = models.CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=GENDER_CHOICES
    )

    user_theme = models.ForeignKey(
        'real_estate.Theme',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

class Message(models.Model):
    text = models.TextField(
        blank=True,
        null=True
    )

    from_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="messages_sent"
    )

    to_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="messages_received",
        null=True,
        blank=True
    )

    image = models.BooleanField()
    
    image_extension = models.CharField(
        blank=True,
        null=True,
        max_length=10
    )

    inserted_at = models.DateTimeField(
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        auto_now=True
    )



